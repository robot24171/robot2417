/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.robot2417;

import java.util.Scanner;

/**
 *
 * @author WIP
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'r' , map);
        Bomb bomb = new Bomb(5, 5);
        map.setBomb(bomb);
        map.setRobot(robot);
        while(true){
            map.showMap();
            // W,a | N,w | E,d | S,s| q: quit
            char direction = inputDirection(kb);
            if(direction=='q'){
                printBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printBye() {
        System.out.println("Bye!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);
    }
}
