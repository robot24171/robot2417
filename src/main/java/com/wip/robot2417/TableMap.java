/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.robot2417;

/**
 *
 * @author WIP
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showTitle();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    showRObot();
                }
                else if(bomb.isOn(x, y)){
                    showBomb();
                }
                else {
                    showDash();
                }
            }
            newLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void newLine() {
        System.out.println("");
    }

    private void showDash() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRObot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        //x->0-(wudth-1), y->0-(height-1)
        return (x >= 0 && x < (width)) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}
