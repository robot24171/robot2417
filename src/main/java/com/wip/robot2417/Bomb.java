/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.robot2417;

/**
 *
 * @author WIP
 */
public class Bomb {

    private int x;
    private int y;
    private char symbol = 'b';

    public Bomb(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }
    
    public boolean isOn(int x, int y){
        return this.x == x && this.y == y;
    }
}
